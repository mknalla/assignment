package com.assignment.bootstarter.controller;

import java.util.ArrayList;
import java.util.List;

import javax.validation.Valid;
import javax.validation.constraints.Pattern;

import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.assignment.bootstarter.utility.ArrayMaker;
import com.assignment.bootstarter.utility.AssignmentUtility;

@RestController
@RequestMapping("api")
public class AssignmentController {
	
    private static final String VALIDATION_REGEX = "^[0-9]";

	@RequestMapping("/Fibonacci")
	public ResponseEntity<Long> fibonacci(@Valid @Pattern(regexp = VALIDATION_REGEX) @RequestParam(value = "n", required = true) Long id) {
		Long fibonacci = AssignmentUtility.getFibonacci(Long.valueOf(id));
	    HttpHeaders responseHeaders = new HttpHeaders();
	    responseHeaders.setPragma("no-cache");
	    ArrayList<String> vary = new ArrayList<String>();
		vary.add("Accept-Encoding");
	    responseHeaders.setVary(vary);
	    responseHeaders.setCacheControl("no-cache");
	    responseHeaders.setExpires(-1);
	    return new ResponseEntity<Long>(fibonacci, responseHeaders, HttpStatus.OK);
		
	}
	
	@RequestMapping("/ReverseWords")
	public ResponseEntity<String> reverseWords(@RequestParam(value = "sentence", required = true) String sentence) {
		String reverseWord = AssignmentUtility.reverseWords(sentence);
		HttpHeaders responseHeaders = new HttpHeaders();
		return new ResponseEntity<String>(reverseWord, responseHeaders, HttpStatus.OK);
	}
	
	@RequestMapping("/TriangleType")
	public ResponseEntity<String> triangleType(@RequestParam(value = "a", required = true) int a, @RequestParam(value = "b", required = true) int b, @RequestParam(value = "c", required = true) int c) {
		String triangleType = AssignmentUtility.triangleType(a, b, c);
		HttpHeaders responseHeaders = new HttpHeaders();
		return new ResponseEntity<String>(triangleType, responseHeaders, HttpStatus.OK);
	}
	
	@RequestMapping(method=RequestMethod.POST, value = "/makeonearray")
	public ResponseEntity<List<Integer>> CombineArray(@RequestBody ArrayMaker arrayMaker) {
		List<Integer> combinedArray = arrayMaker.makeAnArray();
		HttpHeaders responseHeaders = new HttpHeaders();
		return new ResponseEntity<List<Integer>>(combinedArray, responseHeaders, HttpStatus.OK);
	}
}
