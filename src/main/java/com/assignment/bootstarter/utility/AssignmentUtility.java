package com.assignment.bootstarter.utility;

public class AssignmentUtility {

	static AssignmentUtility assignmentUtility = new AssignmentUtility();
	private AssignmentUtility() {
	}
	
	public AssignmentUtility getInstance() {
		return assignmentUtility;
	}
	
	public static Long getFibonacci(long id) {
		if (id == 1)
			return 0L;
		if (id == 2)
			return 1L;

		return getFibonacci(id - 1) + getFibonacci(id - 2);
	}
	
	public static String reverseWords(String sentence) {
		String[] words = sentence.split(" ");
		String reversedString = "";
		for (int i = 0; i < words.length; i++)
	        {
	           String word = words[i]; 
	           String reverseWord = "";
	           for (int j = word.length()-1; j >= 0; j--) 
			   {
				/* The charAt() function returns the character
				 * at the given position in a string
				 */
				reverseWord = reverseWord + word.charAt(j);
			   }
		   reversedString = reversedString + reverseWord + " ";
		}
		return reversedString;
	}
	
	public static String triangleType(int a, int b, int c) {
		String triangleType = "";
		if(a==b && b==c)
			triangleType =  "Equilateral";

        else if(a >= (b+c) || c >= (b+a) || b >= (a+c) )
        	triangleType = "Not a triangle";

        else if ((a==b && b!=c ) || (a!=b && c==a) || (c==b && c!=a))
        	triangleType =  "Isosceles";

        else if(a!=b && b!=c && c!=a)
        	triangleType =  "Scalene";
		
		return triangleType;
	}
}
