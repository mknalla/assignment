package com.assignment.bootstarter.utility;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;

public class ArrayMaker {

	@JsonProperty(value = "Array1")
	List<Integer> array1;
	@JsonProperty(value = "Array2")
	List<Integer> array2;
	@JsonProperty(value = "Array3")
	List<Integer> array3;
	
	public ArrayMaker() {
	}
	
	public ArrayMaker(List<Integer> array1, List<Integer> array2, List<Integer> array3) {
		this.array1 = array1;
		this.array2 = array2;
		this.array3 = array3;
	}
	
	public List<Integer> makeAnArray() {
		List<Integer> allValues = new ArrayList<Integer>();
		allValues.addAll(array1);
		allValues.addAll(array2);
		allValues.addAll(array3);
		
		List<Integer> mergedArray = new ArrayList<Integer>();
		for(Integer value : allValues) {
			if(!mergedArray.contains(value)) {
				mergedArray.add(value);
			}
		}
		
		Collections.sort(mergedArray);
		return mergedArray; 
	}
}
